<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

	Route::get('wishlist/index', [ 
		'as' => 'wishlist.index', 
		'uses' => 'WishlistController@index']);

	Route::get('wishlist/create', [ 
		'as' => 'wishlist.create', 
		'uses' => 'WishlistController@create']);

	Route::post('wishlist',[ 
		'as' => 'wishlist.store', 
		'uses' => 'WishlistController@store']);

	Route::get('wishlist/{id}/destroy', [
			'uses' 	=> 'WishlistController@destroy',
			'as'	=> 'wishlist.destroy'
		]);
		

	Route::resource('profile','ProfileController');

	Route::resource('search','SearchController');
	Route::get('search/{id}/destroy', [
		'uses' 	=> 'SearchController@destroy',
		'as'	=> 'search.destroy'
	]);



Auth::routes();

Route::get('/home', 'HomeController@index');


