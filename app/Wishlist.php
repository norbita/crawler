<?php

namespace crawler;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $table = "wishlist";
	
	protected $fillable = ['article', 'img', 'price', 'user_id' ];

	public static function existe($article,$user)  // relaciones: un articulo ya existe en la lista de deseos de ese usuario
	{
		$resultado = Wishlist::where([
			['article', '=', $article],
			['user_id', '=', $user],
			])->first();
			
		return $resultado;
			
	}	

}
