<?php

namespace crawler\Http\Controllers;

use Illuminate\Http\Request;
use crawler\Wishlist;
use Auth;
use Whishlist;
use App\Http\Requests;


class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if (!Auth::guest()) {
			$contador_wishlist = count(Wishlist::where('user_id', '=', Auth::user()->id)->get());
		} else {
			$contador_wishlist = 0;
		}
        return view('search.create')
		->with('contador_wishlist',$contador_wishlist)
		;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::guest()) {
			$contador_wishlist = count(Wishlist::where('user_id', '=', Auth::user()->id)->get());
		} else {
			$contador_wishlist = 0;
		}
        return view('search.create')
		->with('contador_wishlist',$contador_wishlist)
		;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $url = $request->urlName;
		
		// $output = file_get_contents($url); //otra manera de hacerlo
		
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		curl_close($ch);
		
		$etiqueta = $request->divName;
		$img_tag = "img src";
		$tag_price = "section-title";
		$start = 'alt="';
		$end = '"';
		$price = 0;
		$arreglo = array();
		$notes = explode("\n", $output);
		$contador = count($notes);
		$hay = false;
		for ($i=0; $i < $contador; $i++) {
			if (strpos($notes[$i], $etiqueta) !== false) { // comienza grupo del producto
				$img = null;
				for ($x=0; $x < 5; $x++) {
					if (strpos($notes[$i], $img_tag) !== false) { // encuentra la imagen y el tag
						$img .= $notes[$i];
						$pos = stripos($img, $start); // encuentra el tag
						$str = substr($img, $pos);						
						$str_two = substr($str, strlen($start));
						$second_pos = stripos($str_two, $end);
						$article = substr($str_two, 0, $second_pos);
					}	
					$i++;
					$hay = true;
				}
			}
			if ($hay && strpos($notes[$i], $tag_price) !== false) {
				$price = strip_tags($notes[$i]);
			}
			
			//  price mientras tanto NO SE CONSIGUIO EL PRECIO
			if ($price == 0){
				$price = rand (0, 500);
			}
			// esta pieza hay que quitarla
			
			if ($hay) {
				$existe =false;
				if (!Auth::guest()) {
					$contador_wishlist = count(Wishlist::where('user_id', '=', Auth::user()->id)->get());
					if (Wishlist::existe($article, Auth::user()->id)) {
						$existe = true;
					} 
				} else {
					$contador_wishlist = 0;
				}

				$arreglo[] = array(
					'article' => $article, 
					'img' => $img, 
					'price' => $price, 
					'existe' => $existe
					);
				$price = 0; 
				$hay = false;	
			}
		}
		
		$sort = array();
		foreach($arreglo as $k=>$v) {
			$sort['price'][$k] = $v['price'];
		}

		if ($request->order == 1) {
			array_multisort($sort['price'], SORT_ASC, $arreglo);
			$order = "top 10 cheapest products";
		} else {
			array_multisort($sort['price'], SORT_DESC, $arreglo);
			$order = "top 10 most expensive products";
		}
		$articles  = array_slice($arreglo,0,10);
		

		
		return view ('search.listar')
			->with('articles',$articles)
			->with('order',$order)
			->with('contador_wishlist',$contador_wishlist)
			;
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
