<?php

namespace crawler\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
		//dd($user);
		return view('editProfile')->with('user',$user->id);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$user = Auth::user();
		return view('editProfile')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		Auth::user()->name = $request->name;
		Auth::user()->email = $request->email;
		
		//if ( ! $request->password == '')
		//{
		//	Auth::user()->password = bcrypt($request->password);
		//}
		
		Auth::user()->fresh();
		Auth::user()->save();
		$user = $user = Auth::user();
		return view('editProfile')->with('user', $user);
    }

}
