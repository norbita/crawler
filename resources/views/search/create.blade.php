@extends('layouts.app')

@section('content')
<div class="panel-heading">Search articles</div>
	<!-- numero de articulos en my wishlist -->
	@if (!Auth::guest())
		{!! Form::open(['route' => 'wishlist.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
			<div class="form-group">	
				{!! Form::submit('My wishlist '.'('.$contador_wishlist.')', ['class' => 'btn btn-warning']) !!}
			</div>
		{!! Form::close()  !!}
	@endif
	
<div class="panel-body">
{!! Form::open(['route' => 'search.store', 'method' => 'POST']) !!}
	<div class="col-md-6">
        <div class="form-group">
			{!! Form::label('urlName', 'URL address') !!}	
			{!! Form::text('urlName', 'https://www.appliancesdelivered.ie/search?sort=price_desc',['class' => 'form-control', 'placeholder' => 'URL', 'required']) !!}
		</div>
		
		<div class="form-group">
			{!! Form::label('divName', 'Div name') !!}	
			{!! Form::text('divName', 'product-popup not-active',['class' => 'form-control', 'placeholder' => 'URL', 'required']) !!}
		</div>
		
		<div class="form-group">
			<div class="col-md-6 col-md-offset-4">
				<div class="checkbox">                              
					<br/>
					<label>Order:</label><br/>
					<input type="radio" id="radio01" name="order"  value="0" checked="checked"/>
					<span></span>top 10 most expensive products
					<br/>
					<input type="radio" id="radio02" name="order" value="1" />
					<span></span>top 10 cheapest products
				</div>
			</div>
		</div>

        <div class="form-group">	
			{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
		</div>
		
	</div>
{!! Form::close() !!}
</div>
@endsection