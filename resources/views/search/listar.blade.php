@extends('layouts.app')

@section('title', 'Inventory list')

@section('content')
<div class="panel-heading">{{ $order  }}</div>
	<!-- Buscador de articulos -->
	@if (!Auth::guest())
		{!! Form::open(['route' => 'wishlist.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
			<div class="form-group">	
				{!! Form::submit('My wishlist '.'('.$contador_wishlist.')', ['class' => 'btn btn-warning']) !!}
			</div>
		{!! Form::close()  !!}
	@endif		
	<!-- fin del buscador de tags -->
	<br/>
	<table class="table table-striped">
		<thead>
			<th>Article</th>
			<th></th>
			<th>Price</th>
			@if (!Auth::guest())
				<th>Add to wishlist</th>
			@endif	
		</thead>	
		<tbody>
			@foreach($articles as $article)
				<tr>
					<td>{{ $article['article'] }}</td>
					<td>{!! $article['img'] !!}</td>
					<td>{{ $article['price'] }}</td>
				@if (!Auth::guest())
					@if ($article['existe'])
						<td><span>Added</span></td>
					@else
					<td>
						<a href="{{ route('wishlist.create', $article) }}" class="btn btn-primary"><span>Add</span></a>
					</td>
					@endif
				@endif		
				</tr>
			@endforeach
		</tbody>
	</table>
	<div class="text-center">
		
	</div>

@endsection