@extends('layouts.app')

@section('content')
<div class="panel-heading">Search articles</div>
<div class="panel-body">				 
	<form class="form-horizontal" role="form" method="GET" action="{{ route('search.create') }}">
		{{ csrf_field() }} 
	 
		<div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
			<label for="text" class="col-md-4 control-label">URL address</label>

			<div class="col-md-6">
				<input id="url" type="text" class="form-control" name="url" value="{{ old('url') }}" required autofocus>

				@if ($errors->has('url'))
					<span class="help-block">
						<strong>{{ $errors->first('url') }}</strong>
					</span>
				@endif
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-6 col-md-offset-4">
				<div class="checkbox">                              
					<br/>
					<label>Order:</label>
					<input type="radio" id="radio01" name="order"  value="0" 
					 <?php //if($sexo == 0) {echo 'checked';}?> />
					 <span></span>Ascending
					<input type="radio" id="radio02" name="order" value="1" 
					<?php //if($sexo == 1) {echo 'checked';}?> />
					<span></span>Descending
					<br/><br/>
					@if (!Auth::guest())
						<label>
							<input type="checkbox" name="ascending" {{ old('ascending') ? 'checked' : '' }}> Add to wish List
						</label>
					@endif	
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-8 col-md-offset-4">
				<button type="submit" class="btn btn-primary">
					Search
				</button>

			</div>
		</div>
	</form>
</div>
	
@endsection
