@extends('layouts.app')

@section('title', 'My wish list')

@section('content')
	<div class="panel-heading"></div>
	<!-- Buscador de articulos -->

	<br/>
	<table class="table table-striped">
		<thead>
			<th>Article</th>
			<th></th>
			<th>Price</th>
		</thead>	
		<tbody>
			@foreach($wishlist as $article)
				<tr>
					<td>{{ $article['article'] }}</td>
					<td>{!! $article['img'] !!}</td>
					<td>{{ $article['price'] }}</td>
					
					<td>
						<a href="{{ route('wishlist.destroy',$article->id) }}" onclick="return confirm('¿Seguro deseas eliminar el registro?')" class="btn btn-danger"><span>{{ 'Delete' }}</span></a>
					</td>
					
				</tr>
			@endforeach
		</tbody>
	</table>

@endsection