@extends('layouts.app')

@section('content')
<div class="panel-heading">Add to wish list</div>
<div class="panel-body">
{!! Form::open(['route' => 'wishlist.store', 'method' => 'POST']) !!}

	<div class="col-md-6">
	<div class="form-group">
		{!! Form::label('title', 'Article') !!}
		{!! Form::text('article', $article, ['class' => 'form-control', 'readonly' => 'true']) !!}
		{!! Form::hidden('img', $img ) !!}
		{!!  $img !!}
	</div>
	
	<div class="form-group">
		{!! Form::label('title', 'Price') !!}
		{!! Form::text('price', $price, ['class' => 'form-control', 'readonly' => 'true']) !!}
	</div>

	<div class="form-group">	
		{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
	</div>
		
	</div>
{!! Form::close() !!}
</div>
@endsection