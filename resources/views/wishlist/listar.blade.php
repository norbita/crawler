@extends('layouts.app')

@section('title', 'Inventory list')

@section('content')
<div class="panel-heading">{{ $order  }}</div>
	<!-- Buscador de articulos -->
	@if (!Auth::guest())
		{!! Form::open(['route' => 'search.create', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
				<div class="input-group">
					{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Wishlist',  'aria-describedby' => 'search']) !!}
					<span class="input-group-addon" id="search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
				</div>
		{!! Form::close()  !!}
	@endif		
	<!-- fin del buscador de tags -->
	<br/>
	<table class="table table-striped">
		<thead>
			<th>Article</th>
			<th></th>
			<th>Price</th>
			@if (!Auth::guest())
				<th>Add to wishlist</th>
			@endif	
		</thead>	
		<tbody>
			@foreach($articles as $article)
				<tr>
					<td>{{ $article['article'] }}</td>
					<td>{!! $article['img'] !!}</td>
					<td>{{ $article['price'] }}</td>
				@if (!Auth::guest())
					<td>
						<a href="{{ route('wishlist.create', $article) }}" onclick="return confirm('Add to wishlist?')" class="btn btn-primary"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
					</td>
				@endif		
				</tr>
			@endforeach
		</tbody>
	</table>
	<div class="text-center">
		
	</div>

@endsection