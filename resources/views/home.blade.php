@extends('layouts.app')

@section('content')

	<div class="panel-heading">Home</div>
		<div class="panel-body">
			<form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
				{{ csrf_field() }}

				<div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
					<label for="text" class="col-md-4 control-label">Inventory Crawler</label>	
				</div>
			</form>
		</div>	
	</div>

@endsection
